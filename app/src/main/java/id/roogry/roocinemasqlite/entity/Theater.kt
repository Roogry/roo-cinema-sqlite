package id.roogry.roocinemasqlite.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Theater (
    var id: Int = 0,
    var name: String? = null,
    var roomType: String? = null,
    var facility: String? = null,
    var capacity: Int? = null,
) : Parcelable
