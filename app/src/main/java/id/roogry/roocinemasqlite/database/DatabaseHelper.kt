package id.roogry.roocinemasqlite.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import id.roogry.roocinemasqlite.database.DatabaseContract.TheaterColumns.Companion.CAPACITY
import id.roogry.roocinemasqlite.database.DatabaseContract.TheaterColumns.Companion.FACILITY
import id.roogry.roocinemasqlite.database.DatabaseContract.TheaterColumns.Companion.NAME
import id.roogry.roocinemasqlite.database.DatabaseContract.TheaterColumns.Companion.ROOMTYPE
import id.roogry.roocinemasqlite.database.DatabaseContract.TheaterColumns.Companion.TABLE_NAME
import id.roogry.roocinemasqlite.database.DatabaseContract.TheaterColumns.Companion._ID

internal class DatabaseHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "cinema_database"
        private const val DATABASE_VERSION = 1
        private const val SQL_CREATE_TABLE_THEATER = "CREATE TABLE $TABLE_NAME" +
                " (${_ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
                " $NAME TEXT NOT NULL," +
                " $ROOMTYPE TEXT NOT NULL," +
                " $FACILITY TEXT NOT NULL," +
                " $CAPACITY INTEGER NOT NULL)"
    }

    override fun onCreate(p0: SQLiteDatabase?) {
        p0?.execSQL(SQL_CREATE_TABLE_THEATER)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(p0)
    }
}
