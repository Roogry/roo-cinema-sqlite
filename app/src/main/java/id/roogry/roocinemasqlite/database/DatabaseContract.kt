package id.roogry.roocinemasqlite.database

import android.provider.BaseColumns

internal class DatabaseContract {
    internal class TheaterColumns : BaseColumns {
        companion object {
            const val TABLE_NAME = "theater"
            const val _ID = "_id"
            const val NAME = "name"
            const val ROOMTYPE = "roomType"
            const val FACILITY = "facility"
            const val CAPACITY = "capacity"
        }
    }
}
