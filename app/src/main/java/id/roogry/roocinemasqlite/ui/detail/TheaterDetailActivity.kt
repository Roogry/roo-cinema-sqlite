package id.roogry.roocinemasqlite.ui.detail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.lifecycle.ViewModelProvider
import id.roogry.roocinemasqlite.R
import id.roogry.roocinemasqlite.database.TheaterHelper
import id.roogry.roocinemasqlite.databinding.ActivityTheaterDetailBinding
import id.roogry.roocinemasqlite.databinding.DialogConfirmDeleteBinding
import id.roogry.roocinemasqlite.entity.Theater
import id.roogry.roocinemasqlite.ui.insert.TheaterAddUpdateActivity

class TheaterDetailActivity : AppCompatActivity() {
    private var activityTheaterDetailBinding: ActivityTheaterDetailBinding? = null
    private val binding get() = activityTheaterDetailBinding

    private lateinit var theater: Theater
    private lateinit var theaterHelper: TheaterHelper

    companion object {
        const val EXTRA_THEATER = "extra_theater"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityTheaterDetailBinding = ActivityTheaterDetailBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        theaterHelper = TheaterHelper.getInstance(applicationContext)
        theaterHelper.open()

        theater = intent.getParcelableExtra<Theater>(EXTRA_THEATER) as Theater

        binding?.tvName?.text = "Theater ${theater.name}"
        binding?.tvCapacity?.text = "${theater.capacity} Orang"
        binding?.tvType?.text = theater.roomType
        binding?.tvFacility?.text = theater.facility

        binding?.ivBack?.setOnClickListener {
            finish()
        }

        binding?.ivMore?.setOnClickListener {
            val popupMenu = PopupMenu(this, it)
            popupMenu.menuInflater.inflate(R.menu.update_delete, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.edit -> {
                        val moveAddUpdateIntent = Intent(this, TheaterAddUpdateActivity::class.java)
                        moveAddUpdateIntent.putExtra(
                            TheaterAddUpdateActivity.EXTRA_THEATER,
                            theater
                        )
                        startActivity(moveAddUpdateIntent)
                        finish()
                    }
                    R.id.delete -> {
                        showConfirmDialog()
                    }
                }
                true
            }
            popupMenu.show()
        }
    }

    private fun showConfirmDialog() {
        val builder = AlertDialog.Builder(this, R.style.DialogSecondary)
            .create()
        val dialogBinding = DialogConfirmDeleteBinding
            .inflate(LayoutInflater.from(this))

        builder.setView(dialogBinding.root)
        builder.setCanceledOnTouchOutside(false)

        dialogBinding.txtTitle.text = "Hapus Theater"
        dialogBinding.txtSubtitle.text =
            "Theater ${theater.name} bertipe ${theater.roomType} dengan kapasitas ${theater.capacity} akan dihapus. "
        dialogBinding.txtSubtitle.text =
            "${dialogBinding.txtSubtitle.text} Apakah yakin ingin menghapus theater ini?"

        dialogBinding.btnCancel.setOnClickListener {
            builder.dismiss();
        }

        dialogBinding.btnHapus.setOnClickListener {
            theaterHelper.deleteById(theater.id.toString()).toLong()
            showToast(getString(R.string.deleted))
            finish()
        }

        builder.show()
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        activityTheaterDetailBinding = null
    }
}