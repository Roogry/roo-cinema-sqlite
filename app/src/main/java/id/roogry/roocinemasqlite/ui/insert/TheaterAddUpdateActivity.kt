package id.roogry.roocinemasqlite.ui.insert

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.RadioButton
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import id.roogry.roocinemasqlite.R
import id.roogry.roocinemasqlite.database.DatabaseContract
import id.roogry.roocinemasqlite.database.TheaterHelper
import id.roogry.roocinemasqlite.databinding.ActivityTheaterAddUpdateBinding
import id.roogry.roocinemasqlite.databinding.DialogConfirmTheaterBinding
import id.roogry.roocinemasqlite.entity.Theater
import id.roogry.roocinemasqlite.ui.detail.TheaterDetailActivity

class TheaterAddUpdateActivity : AppCompatActivity() , View.OnClickListener,
    SeekBar.OnSeekBarChangeListener {
    private var _activityTheaterAddUpdateBinding: ActivityTheaterAddUpdateBinding? = null
    private val binding get() = _activityTheaterAddUpdateBinding

    private var theater: Theater? = null
    private var isEdit = false
    private lateinit var theaterHelper: TheaterHelper

    companion object {
        const val EXTRA_THEATER = "extra_theater"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _activityTheaterAddUpdateBinding = ActivityTheaterAddUpdateBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        theaterHelper = TheaterHelper.getInstance(applicationContext)
        theaterHelper.open()

        binding?.btnSave?.setOnClickListener(this)
        binding?.sbCapacity?.setOnSeekBarChangeListener(this)

        theater = intent.getParcelableExtra(EXTRA_THEATER)
        if (theater != null) {
            isEdit = true
            setTheaterData()
        } else {
            theater = Theater()
        }
    }

    private fun setTheaterData() {
        theater?.let {
            binding?.edtName?.setText(it.name)
            binding?.sbCapacity?.progress = it.capacity!!

            when (it.roomType){
                "Regular" -> binding?.rbRegular?.isChecked = true
                "Deluxe" -> binding?.rbDeluxe?.isChecked = true
                "VIP" -> binding?.rbVIP?.isChecked = true
            }

            val facilities = it.facility!!.split(", ").toTypedArray()
            if (binding?.cbBlanket?.text in facilities){
                binding?.cbBlanket?.isChecked = true
            }
            if (binding?.cbCCTV?.text in facilities){
                binding?.cbCCTV?.isChecked = true
            }
            if (binding?.cbDolby?.text in facilities){
                binding?.cbDolby?.isChecked = true
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnSave -> saveTheater()
        }
    }

    private fun saveTheater() {
        resetInvalid()
        if (isAllFieldValid()) {
            theater?.let {
                it.name = binding!!.edtName.text.toString()
                it.capacity = binding!!.sbCapacity.progress

                val selectedRbId = binding!!.rgType.checkedRadioButtonId
                val selectedRb: RadioButton = findViewById(selectedRbId)
                it.roomType = selectedRb.text.toString()
                it.facility = null

                if (binding!!.cbBlanket.isChecked) {
                    if (it.facility == null) it.facility =
                        binding!!.cbBlanket.text.toString() else it.facility += ", " + binding!!.cbBlanket.text
                }

                if (binding!!.cbCCTV.isChecked) {
                    if (it.facility == null) it.facility =
                        binding!!.cbCCTV.text.toString() else it.facility += ", " + binding!!.cbCCTV.text
                }

                if (binding!!.cbDolby.isChecked) {
                    if (it.facility == null) it.facility =
                        binding!!.cbDolby.text.toString() else it.facility += ", " + binding!!.cbDolby.text
                }
            }
            showConfirmDialog()
        }
    }

    private fun isAllFieldValid(): Boolean {
        var isValid = true
        val selectedRbId = binding!!.rgType.checkedRadioButtonId

        if (binding!!.edtName.text.toString().trim().isEmpty()) {
            binding!!.edtName.error = getString(R.string.err_name)
            binding!!.tvInvalidName.visibility = View.VISIBLE
            binding!!.tvLabelName.setTextColor(ContextCompat.getColor(this, R.color.text_danger))
            binding!!.edtName.background = ContextCompat.getDrawable(
                this,
                R.drawable.bg_rounded_light_red
            )
            isValid = false
        }

        if (selectedRbId == -1) {
            binding!!.tvInvalidType.visibility = View.VISIBLE
            binding!!.tvLabelType.setTextColor(ContextCompat.getColor(this, R.color.text_danger))
            isValid = false
        }

        if (binding!!.sbCapacity.progress !in 10..200) {
            binding!!.tvInvalidCapacity.visibility = View.VISIBLE
            binding!!.tvLabelCapacity.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.text_danger
                )
            )
            binding!!.tvCapacity.setTextColor(ContextCompat.getColor(this, R.color.text_danger))
            isValid = false
        }

        return isValid
    }

    private fun resetInvalid() {
        binding!!.edtName.error = null

        binding!!.tvInvalidName.visibility = View.GONE
        binding!!.tvInvalidType.visibility = View.GONE
        binding!!.tvInvalidCapacity.visibility = View.GONE

        binding!!.tvLabelName.setTextColor(ContextCompat.getColor(this, R.color.text_primary))
        binding!!.tvLabelType.setTextColor(ContextCompat.getColor(this, R.color.text_primary))
        binding!!.tvLabelCapacity.setTextColor(ContextCompat.getColor(this, R.color.text_primary))
        binding!!.tvCapacity.setTextColor(ContextCompat.getColor(this, R.color.text_primary))

        binding!!.edtName.background = ContextCompat.getDrawable(
            this,
            R.drawable.bg_rounded_light_grey
        )
    }

    private fun showConfirmDialog() {
        val builder = AlertDialog.Builder(this, R.style.DialogSecondary)
            .create()
        val dialogBinding = DialogConfirmTheaterBinding
            .inflate(LayoutInflater.from(this))

        builder.setView(dialogBinding.root)
        builder.setCanceledOnTouchOutside(false)

        theater?.let {
            dialogBinding.txtSubtitle.text =
                "Theater ${it.name} bertipe ${it.roomType} dengan kapasitas ${it.capacity}. "
        }

        if (isEdit) {
            dialogBinding.txtTitle.text = "Ubah Theater"
            dialogBinding.txtSubtitle.text =
                "${dialogBinding.txtSubtitle.text} Apakah yakin ingin mengubah theater ini?"
        } else {
            dialogBinding.txtTitle.text = "Theater Baru"
            dialogBinding.txtSubtitle.text =
                "${dialogBinding.txtSubtitle.text} Apakah yakin ingin menambahkan theater ini?"
        }

        dialogBinding.btnCancel.setOnClickListener {
            builder.dismiss();
        }

        dialogBinding.btnSave.setOnClickListener {
            theater?.let {
                val values = ContentValues()
                values.put(DatabaseContract.TheaterColumns.NAME, it.name)
                values.put(DatabaseContract.TheaterColumns.ROOMTYPE, it.roomType)
                values.put(DatabaseContract.TheaterColumns.FACILITY, it.facility)
                values.put(DatabaseContract.TheaterColumns.CAPACITY, it.capacity)

                if (isEdit) {
//                    theaterAddUpdateViewModel.update(it)
                    theaterHelper.update(theater?.id.toString(), values)
                    showToast(getString(R.string.changed))

                    val moveDetailTheaterIntent = Intent(this, TheaterDetailActivity::class.java)
                    moveDetailTheaterIntent.putExtra(TheaterDetailActivity.EXTRA_THEATER, theater)
                    startActivity(moveDetailTheaterIntent)
                } else {
                    theaterHelper.insert(values)
                    showToast(getString(R.string.added))
                }
            }

            finish()
        }

        builder.show()
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        binding?.tvCapacity?.text = "$progress Orang"
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        //
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        //
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        _activityTheaterAddUpdateBinding = null
    }
}