package id.roogry.roocinemasqlite.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.roogry.roocinemasqlite.R
import id.roogry.roocinemasqlite.database.TheaterHelper
import id.roogry.roocinemasqlite.databinding.ActivityMainBinding
import id.roogry.roocinemasqlite.entity.Theater
import id.roogry.roocinemasqlite.helper.MappingHelper
import id.roogry.roocinemasqlite.ui.insert.TheaterAddUpdateActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private var activityMainBinding: ActivityMainBinding? = null
    private val binding get() = activityMainBinding
    private lateinit var adapter: TheaterAdapter

    companion object {
        private const val EXTRA_STATE = "EXTRA_STATE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        adapter = TheaterAdapter()

        binding?.rvTheaters?.layoutManager = LinearLayoutManager(this)
        binding?.rvTheaters?.setHasFixedSize(true)
        binding?.rvTheaters?.adapter = adapter

        binding?.fabAdd?.setOnClickListener { view ->
            if (view.id == R.id.fab_add) {
                val intent = Intent(this, TheaterAddUpdateActivity::class.java)
                startActivity(intent)
            }
        }

        if (savedInstanceState == null) {
            loadTheatersAsync()
        } else {
            val list = savedInstanceState.getParcelableArrayList<Theater>(EXTRA_STATE)
            if (list != null) {
                adapter.setListTheaters(list)
            }
        }
    }

    private fun loadTheatersAsync() {
        GlobalScope.launch(Dispatchers.Main) {
            val theaterHelper = TheaterHelper.getInstance(applicationContext)
            theaterHelper.open()
            val deferredTheaters = async(Dispatchers.IO) {
                val cursor = theaterHelper.queryAll()
                MappingHelper.mapCursorToArrayList(cursor)
            }
            val theaters = deferredTheaters.await()
            theaterHelper.close()

            adapter.setListTheaters(theaters)

            if (theaters.size > 0) {
                binding?.emptyHolder?.visibility = View.GONE
            } else {
                binding?.emptyHolder?.visibility = View.VISIBLE
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(EXTRA_STATE, adapter.getListTheaters())
    }

    override fun onResume() {
        super.onResume()
        loadTheatersAsync()
    }

    override fun onDestroy() {
        super.onDestroy()
        activityMainBinding = null
    }
}