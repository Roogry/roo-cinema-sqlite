package id.roogry.roocinemasqlite.helper

import androidx.recyclerview.widget.DiffUtil
import id.roogry.roocinemasqlite.entity.Theater

class TheaterDiffCallback(
    private val oldTheaterList: List<Theater>,
    private val newTheaterList: List<Theater>
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldTheaterList.size
    }

    override fun getNewListSize(): Int {
        return newTheaterList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldTheaterList[oldItemPosition].id == newTheaterList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldTheater = oldTheaterList[oldItemPosition]
        val newTheater = newTheaterList[newItemPosition]
        return oldTheater.name == newTheater.name && oldTheater.roomType == newTheater.roomType && oldTheater.facility == newTheater.facility && oldTheater.capacity == newTheater.capacity
    }
}