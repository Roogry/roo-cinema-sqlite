package id.roogry.roocinemasqlite.helper

import android.database.Cursor
import id.roogry.roocinemasqlite.database.DatabaseContract
import id.roogry.roocinemasqlite.entity.Theater

object MappingHelper {

    fun mapCursorToArrayList(theatersCursor: Cursor?): ArrayList<Theater> {
        val theatersList = ArrayList<Theater>()
        theatersCursor?.apply {
            while (moveToNext()) {
                val id = getInt(getColumnIndexOrThrow(DatabaseContract.TheaterColumns._ID))
                val name = getString(getColumnIndexOrThrow(DatabaseContract.TheaterColumns.NAME))
                val roomType = getString(getColumnIndexOrThrow(DatabaseContract.TheaterColumns.ROOMTYPE))
                val facility = getString(getColumnIndexOrThrow(DatabaseContract.TheaterColumns.FACILITY))
                val capacity = getInt(getColumnIndexOrThrow(DatabaseContract.TheaterColumns.CAPACITY))
                theatersList.add(Theater(id, name, roomType, facility, capacity))
            }
        }
        return theatersList
    }
}